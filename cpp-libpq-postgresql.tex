% Exploitation d'une base de données PostgreSQL en C/C++
%
% pour la compilation de ce document, une connexion Internet
% et un accès au serveur PostgreSQL du lycée sont nécessaires
%
% Auteur  : Grégory DAVID
%%

\documentclass[10pt,a4paper,oneside,titlepage,final]{exam}

\usepackage{style/layout}
\usepackage{style/glossaire}

\newcommand{\MONTITRE}{Exploitation en \texttt{C/C++} avec \texttt{libpq}}
\newcommand{\MONSOUSTITRE}{d'une base de données PostgreSQL}
\newcommand{\DISCIPLINE}{\glsentrytext{SiSIX} -- \glsentrydesc{SiSIX}}

\usepackage[%
pdftex,%
pdfpagelabels=true,%
pdftitle={Exploitation en C/C++ avec libpq},%
pdfauthor={Grégory DAVID},%
pdfsubject={\MONSOUSTITRE},%
colorlinks,%
]{hyperref}
\usepackage{style/commands}
\usepackage{bashful}
\addbibresource{\jobname.bib}
\lstset{%
  basicstyle={\ttfamily\scriptsize},%
}

\title{%
  \begin{flushright}
    \noindent{\Huge {\bf \MONTITRE}} \\
    \noindent{\huge \MONSOUSTITRE} \\
    \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

%\printanswers\groolotPhiligranne{CORRECTION}

\begin{document}
% Page de titre
\maketitle

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\clearpage
\section*{Correspondances avec le Référentiel de certification du BTS
  SIO}
\subsection*{Activités support de l'acquisition des compétences}

\paragraph{D1.1 -- Analyse de la demande}
\begin{itemize}
  \item A1.1.1 Analyse du cahier des charges d'un service à produire
\end{itemize}

\paragraph{D4.1 -- Conception et réalisation d'une solution
  applicative}
\begin{itemize}
  \item A4.1.3 Conception ou adaptation d'une base de données
  \item A4.1.7 Développement, utilisation ou adaptation de composants
  logiciels
  \item A4.1.9 Rédaction d'une documentation technique
\end{itemize}

\paragraph{D5.2 -- Gestion des compétences}
\begin{itemize}
  \item A5.2.1 Exploitation des référentiels, normes et standards
  adoptés par le prestataire informatique
\end{itemize}

\subsection*{Savoirs-faire}
\begin{itemize}
  \item Interpréter un schéma de base de données
  \item Développer et maintenir une application exploitant une base de
  données partagée\footnote{hormis les techniques de gestion
    concurrente des accès}
  \item Élaborer un jeu d'essai
  \item Utiliser des outils de travail collaboratif
\end{itemize}

\subsection*{Savoirs associés}
\begin{itemize}
  \item Architectures applicatives : concepts de base
  \item Techniques de présentation des données et des documents
  \item Techniques de mise au point
  \item Bonnes pratiques de documentation d'une application
\end{itemize}

\clearpage
\section{Contexte}\label{sec:contexte}

Des données ont été stockées dans une structure de données cohérente
dans le SGBDR PostgreSQL. La manipulation et la gestion de ces données
peuvent se faire :
\begin{itemize}
  \item directement avec l'utilitaire \texttt{psql}
  \item avec un logiciel d'architecture 2-tiers tel que
  \texttt{pgadmin3}
  \item avec un logiciel d'architecture 3-tiers tel que
  \texttt{phpPgAdmin}
\end{itemize}

La structure et les données sont fournies dans les \lstlistingname
s~\vrefrange{lst:structure.sql}{lst:data.sql} au format SQL de
PostgreSQL.

\lstinputlisting[float={p}, language=SQL, label={lst:structure.sql},
caption={Structure des données}]{data/structure.sql}

\lstinputlisting[float={p}, language=SQL, label={lst:data.sql},
caption={Données initiales}]{data/data.sql}


\section{Comment bien débuter ?}\label{sec:getting.started}

Selon l'article encyclopédique
\textcite{wikipedia_bibliotheque_logicielle}, en informatique, une
bibliothèque logicielle est une collection de routines, qui peuvent
être déjà compilées et prêtes à être utilisées par des programmes. Les
bibliothèques sont enregistrées dans des fichiers semblables, voire
identiques aux fichiers de programmes, sous la forme d'une collection
de fichiers de code objet rassemblés accompagnée d'un index permettant
de retrouver facilement chaque routine. Le mot "librairie" est souvent
utilisé à tort pour désigner une bibliothèque logicielle. Il s'agit
d'un anglicisme fautif dû à un faux-ami (\emph{library}).

Les bibliothèques sont apparues dans les années 1950, et sont devenues
un sujet incontournable de programmation. Elles sont utilisées pour
réaliser des interfaces de programmation, des \emph{frameworks}, des
\emph{plugins} ainsi que des langages de programmation. Les routines
contenues dans les bibliothèques sont typiquement en rapport avec des
opérations fréquentes en programmation : manipulation des interfaces
utilisateur, manipulation des bases de données ou les calculs
mathématiques.

Les bibliothèques sont manipulées par l'éditeur de lien et le système
d'exploitation. Les manipulations sont différentes suivant que la
bibliothèque est statique ou partagée. Les emplacements et les noms
des bibliothèques varient selon les systèmes d'exploitation.

\subsection{L'utilitaire \texttt{pkg-config}}\label{sec:pkgconfig}

\texttt{pkg-config} est un logiciel qui fournit une interface unifiée
pour interroger les bibliothèques installées lors de la compilation de
code source qui utilise une de ces bibliothèques.

Si le créateur de la bibliothèque a fourni une configuration pour
\texttt{pkg-config}, l'utilitaire permet alors d'obtenir les
informations de configuration de la \emph{compilation} et de
l'\emph{édition des liens} (liaison).

Par exemple, si l'on veut utiliser la bibliothèque \texttt{libpq} --
qui permet d'intégrer à son programme les fonctionnalités
d'interrogation d'une base de données PostgreSQL -- et que le
fournisseur de la bibliothèque a fourni une configuration pour
\texttt{pkg-config}, alors on peut invoquer l'utilitaire pour
connaître la configuration de compilation avec la commande
\texttt{pkg-config -{-}cflags libpq}, et obtenir la configuration de
l'édition des liens (liaison) avec la commande \texttt{pkg-config
  -{-}libs libpq}.

Ces commandes fournissent alors les informations utiles à la
compilation (voir \lstlistingname~\vref{lst:pkgconfig.cflags}) et
l'édition des liens (liaison) (voir
\lstlistingname~\vref{lst:pkgconfig.libs}).

\subsubsection{Exemples d'utilisation}\label{sec:exemples.utilisation}

\paragraph{Configuration de compilation}
L'exemple suivant montre comment nous pouvons obtenir les informations
nécessaires à l'étape de pré-\emph{processing} lorsque nous utilisons
la bibliothèque \texttt{libpq}.

\bash[script,prefix={}]
pkg-config --cflags libpq
\END

Ce qui nous donne les informations suivantes : voir le
\lstlistingname~\vref{lst:pkgconfig.cflags}.

\lstinputlisting[float={p}, label={lst:pkgconfig.cflags},
caption={Informations de configuration de la \textbf{compilation}
  lorsque la bibliothèque \texttt{libpq} est utilisée dans du code
  source en C/C++}]{\jobname.stdout}

Ainsi pour assurer l'étape de compilation de notre programme
\texttt{main.cpp} nous ferions l'appel à \texttt{g++} de la façon
suivante :

\bash[stdout]
echo "g++ $(pkg-config --cflags libpq) -c -o obj/main.o main.cpp"
\END
% $

\paragraph{Configuration de l'édition des liens (liaison)}
L'exemple suivant montre comment nous pouvons obtenir les informations
nécessaires à l'édition des liens lorsque nous utilisons la
bibliothèque \texttt{libpq}.

\bash[script,prefix={}]
pkg-config --libs libpq
\END

Ce qui nous donne les informations suivantes : voir le
\lstlistingname~\vref{lst:pkgconfig.libs}.

\lstinputlisting[float={p}, label={lst:pkgconfig.libs},
caption={Informations de configuration de l'\textbf{édition des liens}
  (liaison) lorsque la bibliothèque \texttt{libpq} est utilisée dans
  du code source en C/C++}]{\jobname.stdout}

Ainsi pour assurer l'étape de l'édition des liens (liaison) de notre
programme \texttt{main.cpp} nous ferions l'appel à \texttt{g++} de la
façon suivante :

\bash[stdout]
echo "g++ -o bin/main obj/main.o $(pkg-config --libs libpq)"
\END
% $

\subsection{La bibliothèque \texttt{libpq} de
  PostgreSQL~\cite{doc_postgresql_libpq}}\label{sec:libpq}

Pour l'élaboration de notre programme, nous partons de l'état le plus
simple d'un programme écrit en C/C++, tel que visible dans le
\lstlistingname~\vref{lst:prog.initial}.

\bash
git show refs/remotes/origin/prog/initial:data/main.cpp
\END

\lstinputlisting[float={p}, language=C++, label={lst:prog.initial},
caption={Programme de base à partir duquel toutes les modifications
  vont s'ajouter}]{\jobname.stdout}


La bibliothèque logicielle fournie par PostgreSQL est la
\texttt{libpq} : voir en ligne la \textcite{doc_postgresql_libpq}.

Elle fourni l'ensemble des routines utiles à la connexion et la
récupération des données depuis un serveur PostgreSQL.

Son intégration dans du code écrit en C/C++ se fait comme n'importe
quelle bibliothèque logicielle externe :

\bash
git diff refs/remotes/origin/prog/initial refs/remotes/origin/prog/include-libpq -- data/main.cpp
\END

\lstinputlisting[float={p}, language=diff,
label={lst:prog.include.libpq}, caption={Inclusion du fichier
  d'en-tête de la bibliothèque \texttt{libpq} permettant d'obtenir les
  prototypes des fonctions déclarées par la
  bibliothèque}]{\jobname.stdout}

\subsection{Analyse sémantique}\label{sec:analyse.semantique}

Pour assurer correctement la construction du programme, le compilateur
va, lors de la phase d'analyse sémantique, vérifier que les fonctions
auxquelles le code source fait appel sont déclarées quelque part.

Par conséquent, la référence au fichier d'en-tête de la \texttt{libpq}
(comme dans le \lstlistingname~\vref{lst:prog.include.libpq}) fournie
les prototypes permettant cette vérification.

Mais pour être inclus, le fichier \texttt{libpq-fe.h} doit être
accessible. Ainsi, nous allons exploiter l'utilitaire
\texttt{pkg-config} (comme présenté préalablement dans
\vref{sec:pkgconfig}) qui nous donne des informations spécifiques
concernant la bibliothèque \texttt{libpq}.

La construction du programme \texttt{main.cpp} devra alors utiliser
les informations de configuration fournie par \texttt{pkg-config}
grâce à : \texttt{pkg-config -{-}cflags libpq}

\bash
g++ $(pkg-config --cflags libpq) -c -o data/main.o data/main.cpp
\END
% $

\bash[stdout]
echo "g++ $(pkg-config --cflags libpq) -c -o obj/main.o main.cpp"
\END
% $

\subsection{Édition des liens}\label{sec:edition-liens}

L'édition des liens, quant à elle, n'est pas plus compliquée que ce
qui a déjà été vu.

\bash
g++ -o data/main data/main.o $(pkg-config --libs libpq)
\END
% $

\bash[stdout]
echo "g++ -o bin/main obj/main.o $(pkg-config --libs libpq)"
\END
% $

Nous pouvons alors vérifier que le programme construit est bien lié à
la bibliothèque \texttt{libpq} en listant les bibliothèques partagées
nécessaires à l'exécution de notre programme avec la commande
\texttt{ldd} invoquée de la manière suivante et dont la sortie est
présentée dans le \lstlistingname~\vref{lst:ldd} :

\bash[stdout]
echo "ldd bin/main"
\END

\bash
ldd data/main | cut -f 2
\END

\lstinputlisting[float={p}, label={lst:ldd}, caption={Liste des
  bibliothèques partagées liées au programme \texttt{main} que nous
  venons de construire, parmis lesquelles nous pouvons retrouver la
  \texttt{libpq}}]{\jobname.stdout}

\clearpage%
\subsection{Travail à faire}\label{sec:taf.compilation}

\begin{questions}
  \question %
  Créer la structure de données telle que fournie dans le
  \lstlistingname~\vref{lst:structure.sql} dans la base de données à
  votre nom sur le serveur PostgreSQL\footnote{utilisez le fichier SQL
    disponible en ligne à l'adresse suivante :\\
    \url{https://framagit.org/sio/si6/\jobname/blob/master/data/structure.sql}}

  \question %
  Intégrer les données depuis le \lstlistingname~\vref{lst:data.sql}
  dans les tables fraîchement créées\footnote{utilisez le fichier SQL
    disponible en ligne à l'adresse suivante :\\
    \url{https://framagit.org/sio/si6/\jobname/blob/master/data/data.sql}}

  \question %
  Créer un programme simple ne faisant rien, comme présenté dans le
  \lstlistingname~\vref{lst:prog.initial}, à enregistrer dans un
  répertoire convenable et à nommer
  \texttt{main.cpp}\footnote{utilisez le fichier C++ disponible en
    ligne à l'adresse suivante :\\
    \url{https://framagit.org/sio/si6/\jobname/blob/master/data/main.cpp}}

  \question %
  Construire le programme exécutable et vérifier que la bibliothèque
  partagée \texttt{libpq} est bien liée
  \begin{parts}
    \part Pour chaque étape de la construction (compilation, édition
    des liens), renseigner un espace documentaire dans le Wiki de
    votre profil sur Framagit
    \part Pour chaque erreur rencontrée, renseigner dans votre Wiki la
    raison de l'erreur et la façon de la corriger
  \end{parts}
  \begin{solution}
\begin{lstlisting}
g++ $(pkg-config --cflags libpq) -c src/main.cpp -o src/main.o
g++ src/main.o -o main $(pkg-config --libs libpq)
ldd main
\end{lstlisting}
  \end{solution}
\end{questions}


\clearpage
\section{Travail à faire}\label{sec:taf}
À l'aide des documentations disponibles dans la
\textcite{doc_postgresql_libpq}, vous réaliserez les tâches décrites
ici.

\subsection{Préambule}\label{sec:preambule}

Vous réaliserez tout d'abord :
\begin{itemize}
  \item une \emph{issue} pour chaque point à résoudre dans l'espace
  projet sur Framagit,
  \item soit :
  \begin{itemize}
    \item la configuration Git de votre répertoire de travail
    \begin{itemize}
      \item Initialisation locale
\begin{lstlisting}
cd chemin/vers/projet
git init
\end{lstlisting}
      \item Définition du dépôt distant
\begin{lstlisting}
git remote add origin https://framagit.org/sio-malraux/si6/sts1/projet.git
\end{lstlisting}
    \end{itemize}
    \item ou bien le clonage du dépôt distant s'il existe déjà avec du
    contenu sur Framagit
\begin{lstlisting}
git clone https://framagit.org/sio-malraux/si6/sts1/projet.git nom_de_la_copie_locale
\end{lstlisting}
  \end{itemize}
  \item Configuration locale (ou globale avec l'option
  \texttt{-{}-global}) de l'auteur et de son adresse de courriel
\begin{lstlisting}
git config user.name "Prénom NOM"
git config user.email "prenom.nom@bts-malraux.net"
\end{lstlisting}
  \item un \emph{commit} Git pour chaque \emph{issue} résolue (où
  \texttt{XX} correspond au numéro de l'\emph{issue}),
\begin{lstlisting}
git commit -m "Description du contenu de la modification" -m "Fixes #XX"
\end{lstlisting}
  \item l'envoie des différents \emph{commits} sur Framagit
\begin{lstlisting}
git push origin master
\end{lstlisting}
\end{itemize}

\subsection{Démarche à suivre}\label{sec:demarche.a.suivre}

Modifier votre code source pour y intégrer les points numérotés
suivants.

Votre production doit correspondre exactement aux affichages
fournis. Vous devrez par conséquent respecter les espacements,
alignements et toute autre structuration de l'information.

\bash
mkdir -p .build
\END

\begin{questions}
  \question %
  Vérifier la connectivité réseau du serveur
  (voir~\vref{sec:verifier.etat.serveur.avant.connexion}). Si le
  serveur n'est pas accessible, afficher le message sur la sortie des
  erreurs tel que dans le
  \lstlistingname{}~\vref{lst:ping-serveur.erreur}, sinon afficher sur
  la sortie standard le message tel que dans le
  \lstlistingname{}~\vref{lst:ping-serveur.ok}.

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/erreur/ping-serveur:data/main.cpp > .build/erreur.ping-serveur.cpp
g++ $(pkg-config --cflags libpq) -o .build/erreur.ping-serveur .build/erreur.ping-serveur.cpp $(pkg-config --libs libpq)
.build/erreur.ping-serveur 2>&1
\END
\lstinputlisting[float={p}, label={lst:ping-serveur.erreur},
caption={Affichage du message sur la sortie des erreurs lorsque la
  connectivité au serveur est défectueuse}]{\jobname.stdout}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/ping-serveur:data/main.cpp > .build/ping-serveur.cpp
g++ $(pkg-config --cflags libpq) -o .build/ping-serveur .build/ping-serveur.cpp $(pkg-config --libs libpq)
.build/ping-serveur 2>&1
\END
\lstinputlisting[float={p}, label={lst:ping-serveur.ok},
caption={Affichage du message sur la sortie standard lorsque la
  connectivité au serveur est assurée}]{\jobname.stdout}

  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/connexion prog/ping-serveur-password
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Si le serveur est joignable, réaliser l'établissement d'une
  connexion vers celui-ci, en prenant soin de ne plus conserver dans
  le code source le mot de passe de connexion
  (voir~\vref{sec:etablissement.connexion}
  et~\vref{sec:protection.mot.de.passe}).
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/ping-serveur-password prog/ping-serveur
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Si la connexion est établie avec le serveur, afficher les
  informations de connexion tel que présentées dans
  le \lstlistingname{}~\vref{lst:connexion.get.infos}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/ping-serveur prog/connexion-get-infos
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/connexion-get-infos:data/main.cpp > .build/connexion_infos.cpp
g++ $(pkg-config --cflags libpq) -o .build/connexion_infos .build/connexion_infos.cpp $(pkg-config --libs libpq)
.build/connexion_infos 2>&1
\END

  \lstinputlisting[float={p}, label={lst:connexion.get.infos},
  caption={Affichage des informations de connexion lorsque la connexion
    s'est bien effectuée}]{\jobname.stdout}

  \question %
  Récupération des données (voir \vref{sec:recuperation.de.donnees}),
  répondants aux critères de sélection suivants :
  \begin{quotation}
    Obtenir uniquement les animaux \emph{Femelles} dont le nom de la
    race est \emph{Singapura}.
  \end{quotation}
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/connexion-get-infos prog/recuperation-donnees
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Validation de l'exécution de la requête en gérant l'ensemble des
  erreurs possibles (voir \vref{sec:verification.etat.donnees.recues}).
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/recuperation-donnees prog/etat-execution
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Afficher les données concernant les résultats obtenus : voir
  \lstlistingname~\vref{lst:affichage.tuples}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/etat-execution prog/affichage-tuples
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/affichage-tuples:data/main.cpp > .build/affichage_tuples.cpp
g++ $(pkg-config --cflags libpq) -o .build/affichage_tuples .build/affichage_tuples.cpp $(pkg-config --libs libpq)
.build/affichage_tuples 2>&1
\END

    \lstinputlisting[float={p}, label={lst:affichage.tuples},
    caption={Affichage des tuples de manière simple}]{\jobname.stdout}

  \question %
  Utiliser des constantes de type caractère pour séparer les champs.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/affichage-tuples prog/separateur-de-champs
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Normaliser la largeur des champs à la longueur du nom de champ le
  plus long : voir \lstlistingname~\vref{lst:normaliser.champs}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/separateur-de-champs prog/normaliser-largeur-champs
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/normaliser-largeur-champs:data/main.cpp > .build/normaliser-largeur-champs.cpp
g++ $(pkg-config --cflags libpq) -o .build/normaliser-largeur-champs .build/normaliser-largeur-champs.cpp $(pkg-config --libs libpq)
.build/normaliser-largeur-champs 2>&1
\END

    \lstinputlisting[float={p}, label={lst:normaliser.champs},
    caption={Normalisation de la largeur des champs, à la longueur du
      champ le plus long}]{\jobname.stdout}

  \question %
  Assurer un alignement des champs sur la gauche : voir
  \lstlistingname~\vref{lst:alignement.champs}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/normaliser-largeur-champs prog/alignement-champs
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/alignement-champs:data/main.cpp > .build/alignement-champs.cpp
g++ $(pkg-config --cflags libpq) -o .build/alignement-champs .build/alignement-champs.cpp $(pkg-config --libs libpq)
.build/alignement-champs 2>&1
\END

    \lstinputlisting[float={p}, label={lst:alignement.champs},
caption={Alignement des champs à gauche}]{\jobname.stdout}

  \question %
  Afficher une en-tête au dessus des données présentant le nom de
  chacun des champs issus de la requête : voir
  \lstlistingname~\vref{lst:en.tete}
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/alignement-champs prog/en-tete
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/en-tete:data/main.cpp > .build/en-tete.cpp
g++ $(pkg-config --cflags libpq) -o .build/en-tete .build/en-tete.cpp $(pkg-config --libs libpq)
.build/en-tete 2>&1
\END

    \lstinputlisting[float={p}, label={lst:en.tete}, caption={Affichage d'une
      en-tête au dessus des données affichées}]{\jobname.stdout}

  \question %
  Affiner la requête SQL pour n'afficher alors que les champs suivants :
  \begin{itemize}
    \item le numéro d'\texttt{id}-entifiant de l'animal,
    \item le \texttt{nom de l'animal},
    \item le \texttt{sexe},
    \item la \texttt{date de naissance},
    \item les \texttt{commentaires},
    \item le nom de la \texttt{race} de l'animal,
    \item la \texttt{description} de la race
  \end{itemize}

  Voir \lstlistingname~\vref{lst:ameliorer.requete.sql}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/en-tete prog/ameliorer-requete-sql
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/ameliorer-requete-sql:data/main.cpp > .build/ameliorer-requete-sql.cpp
g++ $(pkg-config --cflags libpq) -o .build/ameliorer-requete-sql .build/ameliorer-requete-sql.cpp $(pkg-config --libs libpq)
.build/ameliorer-requete-sql 2>&1
\END

    \lstinputlisting[float={p}, label={lst:ameliorer.requete.sql},
    caption={Affichage suite à l'amélioration de la requête
      SQL}]{\jobname.stdout}

  \question %
  Tracer une ligne de séparation entre l'en-tête et les données, dont
  la taille s'adapte au nombre et à la largeur des champs : voir
  \lstlistingname~\vref{lst:ligne.separation}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/ameliorer-requete-sql prog/ligne-separation
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/ligne-separation:data/main.cpp > .build/ligne-separation.cpp
g++ $(pkg-config --cflags libpq) -o .build/ligne-separation .build/ligne-separation.cpp $(pkg-config --libs libpq)
.build/ligne-separation 2>&1
\END
    \lstinputlisting[float={p}, label={lst:ligne.separation}, caption={Ligne
      séparatrice entre l'en-tête et les données}]{\jobname.stdout}

  \question %
  Factoriser (mettre en procédure et/ou fonction) le code du tracer
  de la ligne de séparation pour l'utiliser ensuite dans le dessin
  du tableau de données : voir
  \lstlistingname~\vref{lst:dessiner.tableau}.
\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/dessiner-tableau:data/main.cpp > .build/dessiner-tableau.cpp
g++ $(pkg-config --cflags libpq) -o .build/dessiner-tableau .build/dessiner-tableau.cpp $(pkg-config --libs libpq)
.build/dessiner-tableau 2>&1
\END
    \lstinputlisting[float={p}, label={lst:dessiner.tableau}, caption={Ligne
      séparatrice (dans du code factorisé), avant et après l'en-tête, puis
      après les données}]{\jobname.stdout}

  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/ligne-separation prog/dessiner-tableau
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Le champ \texttt{description} contient des données qui dépassent la
  largeur du champ. Tronquer la chaîne de caractères du champ de
  données à la largeur du champ, en affichant des \texttt{...} à la
  fin pour indiquer qu'une troncature a été réalisée : voir
  \lstlistingname~\vref{lst:tronquer.long.champ}.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/dessiner-tableau prog/tronquer-long-champ
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/prog/tronquer-long-champ:data/main.cpp > .build/tronquer-long-champ.cpp
g++ $(pkg-config --cflags libpq) -o .build/tronquer-long-champ .build/tronquer-long-champ.cpp $(pkg-config --libs libpq)
.build/tronquer-long-champ 2>&1
\END

    \lstinputlisting[float={p}, label={lst:tronquer.long.champ},
    caption={Affichage des champs tronqués si plus long que la largeur du
      nom de champ le plus long}]{\jobname.stdout}

  \question %
  Factoriser votre code source afin de le rendre le plus modulaire
  possible et que la fonction \texttt{main()} soit la plus sobre
  possible. Pour cela, vous envisagerez de répartir dans différents
  sous-programmes (fonctions ou procédures) le code que vous avez
  produit. Vous pourrez penser à des procédures d'affichages
  (\texttt{afficher\_en\_tete(...)}, \texttt{afficher\_donnees(...)},
  \emph{etc.}) ou des fonctions de calculs
  (\texttt{largeur\_maximum\_nom\_champ(...)},
  \texttt{largeur\_maximum\_tableau(...)}, \emph{etc.})
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/tronquer-long-champ prog/factorisation
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Modulariser le programme afin de rassembler les fonctions
  d'affichage d'un côté (dans les fichiers \texttt{affichage.h} et
  \texttt{affichage.cpp}) et les fonctions de calcul de l'autre (dans
  les fichiers \texttt{calculatoire.h} et \texttt{calculatoire.cpp}).
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/factorisation prog/modularisation
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}
\end{questions}

\section{Documentation : Connexion, récupération et exploitation de
  données}

\subsection{Vérifier l'état du serveur avant
  connexion}\label{sec:verifier.etat.serveur.avant.connexion}

Voir la fonction \texttt{PQping}

\subsection{Établissement d'une
  connexion}\label{sec:etablissement.connexion}

Voir la fonction \texttt{PQconnectdb}

\subsection{Protection du mot de
  passe}\label{sec:protection.mot.de.passe}

Voir \url{https://docs.postgresql.fr/9.6/libpq-pgpass.html}

\subsection{Obtention d'informations sur la
  connexion}\label{sec:informations.de.connexion}

Voir la fonction \texttt{PQstatus}

\subsection{Récupération de
  données}\label{sec:recuperation.de.donnees}

Voir la fonction \texttt{PQexec}

\subsection{Vérification de l'état des données
  reçues}\label{sec:verification.etat.donnees.recues}

Voir les fonctions \texttt{PQresultStatus} et \texttt{PQresStatus}

\subsection{Exploitation des
  résultats}\label{sec:exploitation.des.resultats}

\begin{description}
  \item[\texttt{PQntuples}] Fonction qui renvoie le nombre de lignes
  (tuples) du résultat de la requête. Notez que les objets
  \texttt{PGresult} sont limités à un maximum de \texttt{INT\_MAX}
  lignes, donc un résultat de type \texttt{int} est suffisant,
  \item[\texttt{PQnfields}] Fonction qui renvoie le nombre de colonnes
  (champs) de chaque ligne du résultat de la requête,
  \item[\texttt{PQfname}] Fonction qui renvoie le nom de la colonne
  associé avec le numéro de colonne donnée,
  \item[\texttt{PQfnumber}] Fonction qui renvoie le numéro de colonne
  associé au nom de la colonne donné,
  \item[\texttt{PQgetvalue}] Fonction qui renvoie la valeur d'un seul
  champ d'une seule ligne d'un \texttt{PGresult}. Les numéros de
  lignes et de colonnes commencent à zéro,
  \item[\texttt{PQgetisnull}] Fonction qui teste un champ pour savoir
  s'il est nul,
  \item[\texttt{PQgetlength}] Fonction qui renvoie la longueur réelle
  de la valeur d'un champ en octet.
\end{description}


\subsection{Libération des
  résultats}\label{sec:liberation.des.resultats}

\begin{description}
  \item[\texttt{PQclear}] Libère le stockage associé avec un
  \texttt{PGresult}. Chaque résultat de commande devrait être libéré
  via \texttt{PQclear} lorsqu'il n'est plus nécessaire. Vous pouvez
  conserver un objet \texttt{PGresult} aussi longtemps que vous en
  avez besoin ; il ne part pas lorsque vous lancez une nouvelle
  commande, même pas si vous fermez la connexion. Pour vous en
  débarrasser, vous devez appeler \texttt{PQclear}. En cas d'oubli,
  ceci résultera en des pertes mémoires pour votre application.
\end{description}

\subsection{Présentation des
  données}\label{sec:presentation.des.donnees}

Voir \texttt{std::cout}, \texttt{std::setw()}, \texttt{std::setfill()}
et \texttt{std::cout.fill()} dans la
\textcite{doc_cppreference_general}.

\clearpage%
\section{Construction logicielle}\label{sec:construction.logicielle}

À l'aide de la documentation disponible dans \textcite{gnu_make},
ainsi que du didacticiel disponible dans
\textcite{gnu_make_simple_tutorial}, vous assurerez le travail à faire
suivant :

\begin{questions}
  \question %
  Fabriquer le fichier de construction \texttt{Makefile} permettant
  d'assurer :
  \begin{itemize}
    \item la compilation (\texttt{make objets}),
    \item l'édition des liens (\texttt{make programme}),
    \item le nettoyage (\texttt{make clean}).
  \end{itemize}

  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/modularisation prog/makefile
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}
\end{questions}


\section{Bibliothèques
  logicielles}\label{sec:bibliotheques.logicielles}

À l'aide de la documentation disponible
dans~\textcite{tldp_program_library_howto} vous assurerez les travaux à
faire suivant :

\begin{questions}
  \question %
  Fabriquer, à partir des fichiers \texttt{affichage.cpp} et
  \texttt{calculatoire.cpp}, une \textbf{bibliothèque logicielle
    statique} nommée \texttt{libpg\_affichage.a}, et l'utiliser avec
  votre programme en l'incluant dans votre \texttt{Makefile}. Le
  programme résultant devra désormais être nommé
  \texttt{programme-static} et la règle du \texttt{Makefile} devra
  être changée en conséquence.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/makefile prog/lib-static
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}

  \question %
  Fabriquer, à partir des fichiers \texttt{affichage.cpp} et
  \texttt{calculatoire.cpp} et à l'aide de la documentation, une
  \textbf{bibliothèque logicielle partagée} nommée
  \texttt{libpg\_affichage.so}, et l'utiliser avec votre programme en
  l'incluant dans votre \texttt{Makefile}. Le programme résultant
  devra désormais être nommé \texttt{programme-shared} et la règle du
  \texttt{Makefile} devra être nommée en conséquence.
  \begin{solution}
\bash[ignoreStderr,ignoreExitCode]
git diff prog/lib-static prog/lib-shared
\END
    \lstinputlisting[language=diff]{\jobname.stdout}
  \end{solution}
\end{questions}

\ifprintanswers%
\section*{Codes sources du projet}
\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/main.cpp
\END
\lstinputlisting[language={C++},
caption={\texttt{src/main.cpp}}]{\jobname.stdout}%

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/affichage.h
\END
\lstinputlisting[language={C++},
caption={\texttt{src/affichage.h}}]{\jobname.stdout}%
\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/affichage.cpp
\END
\lstinputlisting[language={C++},
caption={\texttt{src/affichage.cpp}}]{\jobname.stdout}%

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/calculatoire.h
\END
\lstinputlisting[language={C++},
caption={\texttt{src/calculatoire.h}}]{\jobname.stdout}%
\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/calculatoire.cpp
\END
\lstinputlisting[language={C++},
caption={\texttt{src/calculatoire.cpp}}]{\jobname.stdout}%

\bash[ignoreStderr,ignoreExitCode]
git show refs/remotes/origin/dev:data/Makefile
\END
\lstinputlisting[language={make},
caption={\texttt{src/Makefile}}]{\jobname.stdout}%
\else%
\fi%


% References bibliographiques
\clearpage%
\printbibheading%
\printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
\printbibliography[check=online,heading=subbibliography,title={Webographiques}]

\printglossaries%

\include{makechangelog}%

\end{document}
